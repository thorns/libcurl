#! /bin/sh
# /*@@
#  @file   libcurl.sh
#  @date   2009-02-05
#  @author Erik Schnetter <schnetter@cct.lsu.edu.de>
#  @desc
#          Setup libcurl as a thorn
#  @enddesc
# @@*/



# /*@@
#   @routine    CCTK_Search
#   @date       Wed Jul 21 11:16:35 1999
#   @author     Tom Goodale
#   @desc
#   Used to search for something in various directories
#   @enddesc
#@@*/

CCTK_Search()
{
  eval  $1=""
  if test $# -lt 4 ; then
    cctk_basedir=""
  else
    cctk_basedir="$4/"
  fi
  for cctk_place in $2
    do
#      echo $ac_n "  Looking in $cctk_place""...$ac_c" #1>&6
      if test -r "$cctk_basedir$cctk_place/$3" ; then
#        echo "$ac_t""... Found" #1>&6
        eval $1="$cctk_place"
        break
      fi
      if test -d "$cctk_basedir$cctk_place/$3" ; then
#        echo "$ac_t""... Found" #1>&6
        eval $1="$cctk_place"
        break
      fi
#      echo "$ac_t"" No" #1>&6
    done

  return
}


# Work out which variation of LIBCURL is installed
# and set the LIBCURL libs, libdirs and includedirs
if [ -z "$LIBCURL_DIR" ]; then
  echo "BEGIN MESSAGE"
  echo 'LIBCURL selected but no LIBCURL_DIR set.  Checking some places...'
  echo "END MESSAGE"

  CCTK_Search LIBCURL_DIR "/usr /usr/local /usr/local/libcurl /usr/local/packages/libcurl /usr/local/apps/libcurl $HOME c:/packages/libcurl" include/curl/curl.h
  if [ -z "$LIBCURL_DIR" ]; then
     echo "BEGIN ERROR" 
     echo 'Unable to locate the LIBCURL directory -- please set LIBCURL_DIR'
     echo "END ERROR" 
     exit 2
  fi
  echo "BEGIN MESSAGE"
  echo "Found a LIBCURL package in $LIBCURL_DIR"
  echo "END MESSAGE"

  # don't explicitely add standard include and library search paths
  if [ "$LIBCURL_DIR" != '/usr' -a "$LIBCURL_DIR" != '/usr/local' ]; then
    LIBCURL_LIB_DIRS="$LIBCURL_DIR/lib"
    LIBCURL_INC_DIRS="$LIBCURL_DIR/include"
  fi
else
  echo "BEGIN MESSAGE" 
  echo "Using LIBCURL package in $LIBCURL_DIR"
  echo "END MESSAGE"

  LIBCURL_LIB_DIRS="$LIBCURL_DIR/lib"
  LIBCURL_INC_DIRS="$LIBCURL_DIR/include"
fi
LIBCURL_LIBS="curl"



# Check whether we run Windows or not
perl -we 'exit (`uname` =~ /^CYGWIN/)'
is_windows=$?



# Finally, add the math lib which might not be linked against by default
if [ $is_windows -eq 0 ]; then
  LIBCURL_LIBS="$LIBCURL_LIBS m"
fi

echo "INCLUDE_DIRECTORY $LIBCURL_INC_DIRS"
echo "LIBRARY $LIBCURL_LIBS"
echo "LIBRARY_DIRECTORY $LIBCURL_LIB_DIRS"

# Remember LIBCURL_DIR setting so that it can be used in later reconfigs
echo "BEGIN MAKE_DEFINITION"
echo "LIBCURL_DIR = $LIBCURL_DIR"
echo "END MAKE_DEFINITION"
